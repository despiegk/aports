# Contributor: Will Sinatra <wpsinatra@gmail.com>
# Maintainer: Will Sinatra <wpsinatra@gmail.com>
pkgname=amazon-ssm-agent
pkgver=3.3.131.0
pkgrel=0
pkgdesc="Amazon SSM Agent for managing EC2 Instances using the SSM APIs."
url="https://aws.amazon.com/documentation/systems-manager/"
license="Apache-2.0"
arch="all !s390x !riscv64 !ppc64le"
# armv7/armhf/x86 complains about cgo linking not being enabled
makedepends="go gcompat bash cgo"
source="https://github.com/aws/amazon-ssm-agent/archive/$pkgver/amazon-ssm-agent-$pkgver.tar.gz
	amazon-ssm-agent.initd
	001_sanitize_makefile.patch"
subpackages="$pkgname-openrc"
options="!check"
_binaries="amazon-ssm-agent
	ssm-agent-worker
	ssm-cli
	ssm-document-worker
	ssm-session-logger
	ssm-session-worker
	ssm-setup-cli
	"

case "$CARCH" in
	armhf) build_arch=arm ;;
	armv7) build_arch=arm ;;
	x86) build_arch=386 ;;
	x86_64) build_arch=amd64 ;;
	aarch64) build_arch=arm64 ;;
esac

build() {
	make build-linux-$build_arch
}

package() {
	for bin in $_binaries; do
		install -Dm0755 "$builddir"/bin/linux_$build_arch/$bin "$pkgdir"/usr/bin/$bin
	done

	install -Dm0644 "$builddir"/bin/amazon-ssm-agent.json.template \
		"$pkgdir"/etc/amazon/ssm/amazon-ssm-agent.json.template
	install -Dm0644 "$builddir"/bin/seelog_unix.xml \
		"$pkgdir"/etc/amazon/ssm/seelog.xml
	install -Dm0644 "$builddir"/bin/seelog_windows.xml.template \
		"$pkgdir"/etc/amazon/ssm/seelog_windows.xml.template

	install -Dm0755 "$srcdir"/amazon-ssm-agent.initd \
		"$pkgdir"/etc/init.d/amazon-ssm-agent
}

sha512sums="
b4c40aabc8da3b5ea3944e420fbe795d0988c57a1eafe8fc6bef6e43f9bf8fb7806213dac7bebe9e5e7e6413a2ddb4057820bcbc399c4c92f6a3ba0078c14bd7  amazon-ssm-agent-3.3.131.0.tar.gz
2c07d6777ad0d3994f09838818bff2c0bc061238e2c3ca757a9bd04cd25924ce24bdc2ecd7edbd4b10717ba3c8806398626e0718acf8e7ba5c66853369b20f48  amazon-ssm-agent.initd
d90a10ed7f3cf0a58bb73f737c2d10d56b3942886535df7d9215e2cf809c59ef708ed4d9775062e46343cf64726d25adaf048c86f39d4a7647deebd91d69c2f9  001_sanitize_makefile.patch
"
