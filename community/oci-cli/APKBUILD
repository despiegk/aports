# Contributor: Adam Bruce <adam@adambruce.net>
# Maintainer: Duncan Bellamy <dunk@denkimushi.com>
pkgname=oci-cli
pkgver=3.38.1
pkgrel=0
pkgdesc="Oracle Cloud Infrastructure CLI"
url="https://docs.oracle.com/en-us/iaas/Content/API/Concepts/cliconcepts.htm"
arch="noarch"
license="UPL-1.0 OR Apache-2.0"
depends="
	python3
	py3-arrow
	py3-certifi
	py3-click
	py3-cryptography
	py3-dateutil
	py3-jmespath
	py3-oci
	py3-openssl
	py3-prompt_toolkit
	py3-setuptools
	py3-six
	py3-terminaltables
	py3-tz
	py3-yaml
	"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/oracle/oci-cli/archive/refs/tags/v$pkgver.tar.gz"
options="!check" # Cannot test as OCI resource identifiers are required as environment variables

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl

	find "$pkgdir"/usr/lib/python* -type d -name "tests" -exec rm -r {} \+
}

sha512sums="
a97944980146d9bf4b58a34a80e1f5fa9d8f6cb2e8543c3b314502ed6c498c9ef8954c1492e37974cc5adfbb9ce95c0a556bc009f6c2d9ac5b27496e525c0163  oci-cli-3.38.1.tar.gz
"
